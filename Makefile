USER=$(shell id -u $$USER)
CMD_DJANGO_MANAGE=$(shell docker-compose run web python manage.py )
DB_DOCKER=$(shell docker ps -q -f name=db)
DB_USER=postgres

run:
	docker-compose up

migrate:
	docker-compose run web python manage.py makemigrations $(name)
	docker-compose run web python manage.py migrate $(name)

create-project:
	docker-compose run web django-admin startproject $(name) .
	sudo chown -R ${USER}:${USER} app
	@echo "Your Django project was created. If you want to use PostgreSQL as a database,"
	@echo "please change your settings.py: "
	@echo "DATABASES = {"
	@echo "    'default': {"
	@echo "        'ENGINE': 'django.db.backends.postgresql_psycopg2',"
	@echo "        'NAME': 'myprojectname',"
	@echo "        'USER': 'myprojectuser',"
	@echo "        'PASSWORD': 'password',"
	@echo "        'HOST': 'db',"
	@echo "        'PORT': '5432',"
	@echo "    }"
	@echo "}"
	docker-compose down

create-superuser:
	make migrate
	docker-compose run web python manage.py createsuperuser 
	make run

create-app:
	docker-compose run web python manage.py startapp $(name) 

shell:
	docker-compose run web python manage.py shell

psql:
	docker exec -ti $(DB_DOCKER) psql -U $(DB_USER) 

test:
	docker-compose run web python manage.py test
