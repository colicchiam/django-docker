# Quickstart

clone this repo and type `make create-project name=yourproject`

> It will create your project, give correct right for your code's volume and stop all containers.

Then follow the instructions on the settings.py

Finally type `make create-superuser`

> It will create your superuser in the database and you can go to http://localhost:8000 to check out your dockerised Django.
